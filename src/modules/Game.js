import World from './World'
import config from '../config'

class Game {

    constructor(wrapper) {
        this.wrapper = wrapper;
    }

    Start() {
        this.world = new World({
            canvas: this.wrapper.$refs.canvas,
            wrapper: this.wrapper,
            sceneId: this.wrapper.$route.params.sceneId,
            endpoint: config.endpoint
        })
    }
}

export default Game