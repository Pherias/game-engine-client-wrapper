import { Entity, Animation, Physics } from 'game-engine-client-package'

import PlayerIdleRightSprite1 from '../assets/sprites/player_right_1.png'
import PlayerIdleRightSprite2 from '../assets/sprites/player_right_2.png'
import PlayerIdleLeftSprite1 from '../assets/sprites/player_left_1.png'
import PlayerIdleLeftSprite2 from '../assets/sprites/player_left_2.png'

import PlayerWalkRightSprite1 from '../assets/sprites/player_walk_right_1.png';
import PlayerWalkRightSprite2Center from '../assets/sprites/player_walk_right_2_center.png';
import PlayerWalkRightSprite3 from '../assets/sprites/player_walk_right_3.png';
import PlayerWalkLeftSprite1 from '../assets/sprites/player_walk_left_1.png';
import PlayerWalkLeftSprite2Center from '../assets/sprites/player_walk_left_2_center.png';
import PlayerWalkLeftSprite3 from '../assets/sprites/player_walk_left_3.png';

const PlayerIdleRight = [PlayerIdleRightSprite1, PlayerIdleRightSprite2];
const PlayerIdleLeft = [PlayerIdleLeftSprite1, PlayerIdleLeftSprite2];
const PlayerWalkLeft = [PlayerWalkLeftSprite1, PlayerWalkLeftSprite2Center, PlayerWalkLeftSprite3, PlayerWalkLeftSprite2Center];
const PlayerWalkRight = [PlayerWalkRightSprite1, PlayerWalkRightSprite2Center, PlayerWalkRightSprite3, PlayerWalkRightSprite2Center];

const sprite = new Image();

class Player extends Entity {

    constructor(scene) {

        super(scene, { 
            type: 'Player', 
            correctionFrequency: 1,
            sorting: 2,
        });

        this.xVelocity = 0;
        this.yVelocity = 0;
        this.lastDirectionX = 1;

        this.speed = 5;

        this.width = 32;
        this.height = 32;

        this.sprite = new Image();
        this.animation = new Animation(this)
            .AddAnimation('idle_right', PlayerIdleRight, 250)
            .AddAnimation('idle_left', PlayerIdleLeft, 250)
            .AddAnimation('walk_right', PlayerWalkRight, 150)
            .AddAnimation('walk_left', PlayerWalkLeft, 150)
        
        this.animation.Trigger('idle_right')

        this.physics = new Physics(this)
            // .AddCollision(32, 32)
    }

    GetSprite() {
        if (!this.animation) return {
            image: PlayerIdleRightSprite1,
            sprite
        }

        const image = this.animation.GetCurrentFrame();

        return {
            sprite,
            image
        }
    }

    Start() {

    }

    Update() {
        this.Move();

        if (!this.isMine) return;
    }

    OnKeyDown(e) {
        if (!this.isMine) return;

        let newXVelocity = this.xVelocity;
        let newYVelocity = this.yVelocity;

        switch (e.key) {
            case 'w': newYVelocity = -1; break;
            case 'a': newXVelocity = -1; break;
            case 's': newYVelocity = 1; break;
            case 'd': newXVelocity = 1; break;
        }

        if (this.xVelocity !== newXVelocity || this.yVelocity !== newYVelocity)
            this.scene.RPC(this, 'RPC_UpdateVelocity', 0, { xVelocity: newXVelocity, yVelocity: newYVelocity })
    }

    OnKeyUp(e) {
        if (!this.isMine) return;       

        switch (e.key) {
            case 'w': if (!this.scene.pressing['s']) this.yVelocity = 0; break;
            case 'a': if (!this.scene.pressing['d']) this.xVelocity = 0; break;
            case 's': if (!this.scene.pressing['w']) this.yVelocity = 0; break;
            case 'd': if (!this.scene.pressing['a']) this.xVelocity = 0; break;
        }

        this.scene.RPC(this, 'RPC_UpdateVelocity', 0, { xVelocity: this.xVelocity, yVelocity: this.yVelocity })
    }

    Move() {
        this.x += this.xVelocity * 1 * this.speed;
        this.y += this.yVelocity * 1 * this.speed;
    }

    RPC_UpdateVelocity(translation) {
        const { xVelocity, yVelocity } = translation;

        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;

        if (xVelocity !== 0)
            this.lastDirectionX = xVelocity;

        if (xVelocity === 0 && yVelocity === 0)
            this.animation.Trigger(this.lastDirectionX < 0 ? 'idle_left' : 'idle_right')
        else
            this.animation.Trigger(xVelocity < 0 ? 'walk_left' : 'walk_right');
    }
}

export default Player;