import { Entity, Physics } from 'game-engine-client-package'

import HouseSprite from '../assets/sprites/house.png'

const sprite = new Image();

class House extends Entity {
    constructor(scene) {

        super(scene, { type: 'House', sorting: 1 })

        this.width = 200;
        this.height = 100;

        this.physics = new Physics(this)
            // .AddCollision(400, 200)
    }

    GetSprite() {
        return {
            sprite,
            image: HouseSprite
        }
    }

    Update() {
        
    }
}

export default House;