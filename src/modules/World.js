import { Physics, Scene } from 'game-engine-client-package'

import Player from "./Player";
import House from "./House"

class World extends Scene {
    localPlayer;

    constructor(config) {
        super('test-world', config, {
            Player,
            House
        });

    }

    OnConnect({ connections }) {
        console.log(`succesfully connected, ${connections.length} connection(s) currently in the scene`);
        if (this.isMaster) console.log('you are master-client');

        this.localPlayer = new Player(this);

        this.Instantiate(this.localPlayer, 100, 100);

        if (!this.isMaster) return;

        const house = new House(this);
        this.Instantiate(house, 200, 200);

        const house2 = new House(this);
        this.Instantiate(house2, 500, 250);
    }

    LoadScene() {

    }

    UpdateDraw() {
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.canvasContext.fillStyle = '#76cb78';
        this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height);

        const camera = Physics.GetRectangleBounds(this.localPlayer.x, this.localPlayer.y, -(this.canvas.width / 2), -(this.canvas.height / 2), this.canvas.width, this.canvas.height)

        for (let i = 0; i < this.entities.length; i++) {
            const entity = this.entities[i];

            const { sprite, image } = entity.GetSprite();
            const { x, y, width, height } = entity;

            const bounds = Physics.GetRectangleBounds(x, y, 0, 0, width, height);

            if (
                ((bounds.topLeft.x + width) < camera.topLeft.x) ||
                ((bounds.topLeft.y + height) < camera.topLeft.y) ||
                ((bounds.bottomRight.x - width) > camera.bottomRight.x) ||
                ((bounds.bottomRight.y - height) > camera.bottomRight.y)
            ) continue;

            sprite.src = image;

            this.canvasContext.drawImage(sprite, x - camera.topLeft.x, y - camera.topLeft.y, width, height);
        }
    }

    Update() {
        this.UpdateDraw();
    }
}

export default World;