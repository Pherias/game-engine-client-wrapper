import Vue from 'vue'
import Router from 'vue-router'
import BaseView from '@/components/BaseView'
import Game from '@/components/Game'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BaseView',
      component: BaseView,
      children: [
        {
          path: '/game/:sceneId',
          name: 'Game',
          component: Game
        }
      ]
    }
  ],
  mode: 'history'
})
